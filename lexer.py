import argparse
import io

from ppf import Parser
from ppf.group import And
from ppf.group import Multiplicity
from ppf.group import Or
from ppf.group import Ref

__author__ = 'maxime'

parser = argparse.ArgumentParser(
    description='Merge multiple files into one using a dependency resolver.')
parser.add_argument('inputFileName', metavar='INPUT FILE', type=str, nargs='+',
                    help='The input file name to parse.')
parser.add_argument('outputFileName', metavar='OUTPUT FILE', type=str, nargs=1,
                    help='The generated file name.')
parser.add_argument('-l', '--licence', metavar='LICENCE FILE', type=str,
                    nargs=1, help='The licence file to include.')

args = parser.parse_args()

def on_document(value, vals):
    return ''


def on_string(value, vals):
    return vals[0].value


def on_token_name(value, vals):
    return vals[0].value


def on_token(value, vals):
    return ''


def on_doc_comment(value, vals):
    return vals[0].value


pre = Parser([
    ('COMMENT', r'/\*.*?\*/', False),
    ('DOC_COMMENT', r'/\*\*(?:\s|\S)*?\*/', True),
    ('DQ_STRING', r'"(?:(?:\\")|[\s\S])*?"', True),
    ('SQ_STRING', r'\'(?:(?:\\\')|[\s\S])*?\'', True),
    ('COLON', r':', False),
    ('EQ', r'=', False),
    ('END', r';', False),
    ('ID', r'[a-zA-Z_]+', True),
    ('NL', r'\n', False),
    ('SKIP', r'[ \t]+', False),
    ('IGNORED', r'.', False),
], {}, [
    ('document', Multiplicity(Or([Ref('token'), 'COMMENT']), 0, -1),
     on_document),
    ('string', Or(['SQ_STRING', 'DQ_STRING']), on_string),
    ('token_name', Or([Ref('string'), 'ID']), on_token_name),
    ('token', And(
        [Multiplicity(Ref('doc_comment'), 0, 1), Ref('token_name'), 'COLON', 'EQ',
         Multiplicity(Ref('string'), 1, -1), 'END']), on_token),
    ('doc_comment', And(['DOC_COMMENT']), on_doc_comment)

])

for input_file_name in args.inputFileName:
    print("FILE: " + input_file_name)
    with io.open(input_file_name) as f:
        pre.parse(f.read())
