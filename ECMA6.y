%%
/**/
%%

var_declaration:
  VAR ID (EQ expression+)* SEMICOLON;
  
expression:
  ID | string | math_expression | boolean_expression | call_expression;
  
string:
  SQ_STRING | DQ_STRING;

math_expression:
  math_expression_element ( STAR | PLUS | SLASH | MINUS | PERCENT ) math_expression_element;

math_expression_element:
  NUMBER | ID | math_expression;




%%

%%
  
/**/
