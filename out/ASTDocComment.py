from ppf.AST.ASTNode import ASTNode

class ASTDocComment(ASTNode):
    def __init__(self, parent, value):
        """
        Construct an ASTDocComment ASTNode object.
        
        :param ASTNode parent: Parent Node in the AST.
        :param str value: The node value.
        """
        super().__init__(parent, 'doc_comment', value)
        pass

    def post_init(self):
        """
        Calls after the initialization phase. When a node is done, all it's
        children's post_init method will be call.
        """
        pass

    def code_gen(self):
        """
        This method generate the backend code.
        """
        pass

