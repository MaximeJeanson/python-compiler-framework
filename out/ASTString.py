from ppf.AST.ASTNode import ASTNode

class ASTString(ASTNode):
    def __init__(self, parent, value):
        """
        Construct an ASTString ASTNode object.
        
        :param ASTNode parent: Parent Node in the AST.
        :param str value: The node value.
        """
        super().__init__(parent, 'string', value)
        pass

    def post_init(self):
        """
        Calls after the initialization phase. When a node is done, all it's
        children's post_init method will be call.
        """
        pass

    def code_gen(self):
        """
        This method generate the backend code.
        """
        pass

