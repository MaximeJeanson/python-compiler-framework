
document:
%type('ROOT');
  (Rule.rule | option | section)*
;

option:
%test('lol');
  PERCENT ID LP args? RP END
;

args:
  arg (COMMAS arg)*
;

arg:
  ID
| NUMBER
| SQ_STRING
| DQ_STRING
;

section:
ID LCB statements RCB;

namespace Rules {
  rule:
    ID COLON option* rule_def END
  ;

  rule_def:
    rule_group+ (OR rule_group+)*
  ;

  rule_group:
    (rule_element | rule_block)+
  ;

  rule_block:
    LP rule_def RP rule_multiplicity?
  ;

  rule_element:
    ID rule_multiplicity?
  ;

  rule_multiplicity:
    PLUS
  | STAR
  | QMARK
  | LB NUMBER (COMMAS NUMBER)? RB
  ;
}

namespace Rule {
  statements:
    rule+
  ;
}
