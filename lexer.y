import options;

comment:
  %ContextFree();
  COMMENT
| LINE_COMMENT
;

document:
  (token | COMMENT | section)*
;

string:
  SQ_STRING
| DQ_STRING
;

doc_comment:
  DOC_COMMENT;

section:
  ID LCB statements RCB;

comment:
  %ContextFree();
  COMMENT
| LINE_COMMENT
;

document:
  (token | COMMENT | section)*
;

string:
  SQ_STRING
| DQ_STRING
;

doc_comment:
  DOC_COMMENT;

section:
  ID LCB statements RCB;

comment:
  %ContextFree();
  COMMENT
| LINE_COMMENT
;

document:
  (token | COMMENT | section)*
;

string:
  SQ_STRING
| DQ_STRING
;

doc_comment:
  DOC_COMMENT;

section:
  ID LCB statements RCB;

comment:
  %ContextFree();
  COMMENT
| LINE_COMMENT
;

document:
  (token | COMMENT | section)*
;

string:
  SQ_STRING
| DQ_STRING
;

doc_comment:
  DOC_COMMENT;

section:
  ID LCB statements RCB;

comment:
  %ContextFree();
  COMMENT
| LINE_COMMENT
;

document:
  (token | COMMENT | section)*
;

string:
  SQ_STRING
| DQ_STRING
;

doc_comment:
  DOC_COMMENT;

section:
  ID LCB statements RCB;

comment:
  %ContextFree();
  COMMENT
| LINE_COMMENT
;

document:
  (token | COMMENT | section)*
;

string:
  SQ_STRING
| DQ_STRING
;

doc_comment:
  DOC_COMMENT;

section:
  ID LCB statements RCB;

namespace lexer {
  statements:
    token_name+
  ;

  token_name:
    string | ID
  ;

  token:
    doc_comment? token_name COLON EQ string+ END
  ;

  statements:
    (token | COMMENT | doc_comment)*
  ;
}
