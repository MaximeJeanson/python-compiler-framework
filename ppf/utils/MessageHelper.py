from enum import Enum
from termcolor import cprint

from ppf.ast.Token import Token
from ppf.TranslationUnit.TranslationUnit import TranslationUnit

__author__ = 'Maxime Jeanson'

class Region:
    def __init__(self, start: int, end: int):
        self.start = start
        self.end = end


class MessageType(Enum):
    Note = 0
    Info = 1
    Warning = 2
    Error = 3
    Fatal = 4


class MessageHelper:
    @staticmethod
    def colors():
        return [
            'white',
            'white',
            'magenta',
            'red',
            'yellow'
        ]

    @staticmethod
    def attributes():
        return [
            ['bold', 'dark'],
            ['bold', 'dark'],
            ['bold', 'blink'],
            ['bold', 'dark'],
            ['bold', 'dark'],
        ]

    default_region = Region(-1, -1)

    @staticmethod
    def print(message, message_type, token, translation_unit,
              erroneous_scope=default_region, erroneous_region=default_region,
              suggestion=''):
        """

        :param message:
        :type message: str
        :param message_type:
        :type message_type: MessageType
        :param token:
        :type token: Token
        :param translation_unit:
        :type translation_unit: TranslationUnit
        :param erroneous_scope:
        :type erroneous_scope: Region
        :param erroneous_region:
        :type erroneous_region: Region
        :param suggestion:
        :type suggestion: str
        """
        cprint('%s:%d:%d: ' % (translation_unit.file_name, token.line, token.column), attrs=['bold', 'blink'], end='')
        cprint('%s: ' % message_type.name, MessageHelper.colors()[message_type.value],
               attrs=MessageHelper.attributes()[message_type.value], end='')
        cprint('%s' % message, attrs=['bold', 'blink'])

        if erroneous_region is not MessageHelper.default_region:
            cprint(translation_unit.lines[token.line - 1].replace('\n', ''))
            cprint(('{0: <%d}{0:~<%d}{0:^<%d}{0:~<%d}' % (erroneous_scope.start, (erroneous_region.start - erroneous_scope.start), (erroneous_region.end - erroneous_region.start), ((erroneous_scope.end - erroneous_scope.start) - (erroneous_region.end - erroneous_region.start)))).format(''))
            if suggestion is not '':
                cprint(suggestion)
