from ppf.ast.ASTNode import ASTNode

__author__ = 'maxime'


def print_attributes(node: ASTNode):
    attributes = ''
    for attribute in node.attributes:
        if node.__getattribute__(attribute) is not "":
            attributes += ' %s="%s"' % (attribute, node.__getattribute__(attribute))

    return attributes


def print_level(level: int, node: ASTNode):
    for i in range(0, level):
        print('  ', end='')

    if node.value is None:
        node.value = ''

    if node.value != "":
        print('<' + node.tag + ' value="' + node.value + '"' + print_attributes(node) + '>')
    else:
        print('<' + node.tag + print_attributes(node) + '>')
    if len(node.children) is not 0:
        for child in node.children:
            if len(child.children) == 0:
                for i in range(0, level + 1):
                    print('  ', end='')

                print(
                    '<' + child.tag + print_attributes(child) + '>' + child.value + '</' + child.tag + '>')
            else:
                print_level(level + 1, child)

    for i in range(0, level):
        print('  ', end='')
    print('</' + node.tag + '>')
