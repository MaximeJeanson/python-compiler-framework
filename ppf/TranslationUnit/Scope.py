from .SymbolTable import SymbolTable

__author__ = 'Maxime Jeanson'

class Scope:
    """
    :type parent: None | Scope
    :type translation_unit: ppf.TranslationUnit.TranslationUnit.TranslationUnit
    :type name: str
    :type symbols: ppf.TranslationUnit.SymbolTable.SymbolTable
    """
    def __init__(self, _parent, _translation_unit, _name):
        """

        :type _parent: None | Scope
        :type _translation_unit: ppf.TranslationUnit.TranslationUnit.TranslationUnit
        :type _name: str
        """
        self.parent = _parent
        self.translation_unit = _translation_unit
        self.name = _name
        self.symbols = SymbolTable(self)
