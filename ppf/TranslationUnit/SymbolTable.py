from .Symbol import Symbol
from ppf.utils.MessageHelper import MessageHelper, Region, MessageType

__author__ = 'Maxime Jeanson'

class SymbolTable:
    """

    :type scope: ppf.TranslationUnit.Scope.Scope
    :type __symbols__: dict[str, ppf.TranslationUnit.Symbol.Symbol]
    """

    @property
    def symbols(self):
        """

        :rtype : dict[str, ppf.TranslationUnit.Symbol.Symbol]
        """
        return self.__symbols__

    def __init__(self, scope):
        """

        :type scope: ppf.TranslationUnit.Scope.Scope
        """
        self.scope = scope
        self.__symbols__ = dict()

    def __add__(self, _symbol):
        """

        :type _symbol: ppf.TranslationUnit.Symbol.Symbol
        """
        if not self.already_define(_symbol):
            self.__symbols__[_symbol.signature] = _symbol

    def add(self, scope):
        """

        :type scope: ppf.TranslationUnit.Symbol.Symbol
        :return: :rtype: None
        """
        return self.__add__(scope)

    def already_define(self, _symbol: Symbol):
        """

        :type _symbol: ppf.TranslationUnit.Symbol.Symbol
        :return: :rtype: bool
        """
        if self.__symbols__.__contains__(_symbol.signature):
            symbol = self.__symbols__[_symbol.signature]
            MessageHelper.print('%s symbol \'%s\' already defined.' % (_symbol.type.name.capitalize(), _symbol.name),
                                MessageType.Warning, _symbol.node.tokens[0], self.scope.translation_unit, Region(1, 4),
                                Region(2, 3))

            MessageHelper.print('Here: ', MessageType.Info, symbol.node.tokens[0], self.scope.translation_unit,
                                Region(1, 4), Region(2, 3))

            return True

        return False
