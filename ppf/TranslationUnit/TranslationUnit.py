import io

__author__ = 'Maxime Jeanson'

class TranslationUnit:
    """
    :type file_name: str
    :type parser: None or ppf.Parser.Parser
    :type ast: None or ppf.ast.ASTNode.ASTNode
    :type file: io.FileIO
    :type lines: list[str]
    """
    def __init__(self, _file_name, _parser, _ast):
        """

        :type _file_name: str
        :type _parser: None or ppf.Parser.Parser
        :type _ast: None or ppf.ast.ASTNode.ASTNode
        """
        self.file_name = _file_name
        self.parser = _parser
        self.ast = _ast
        self.file = io.open(_file_name)
        self.lines = self.file.readlines()
