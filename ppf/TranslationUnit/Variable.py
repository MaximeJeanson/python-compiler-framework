from .TranslationUnit import TranslationUnit

__author__ = 'maxime'

class Type:
    def __init__(self,
                 _name: str,
                 _sig: str):
        self.name = _name
        self.signature = _sig
        self.is_class = _sig[0] == 'c'
    type_table = dict()

    @staticmethod
    def register_type(_type):
        if not isinstance(_type, Type):
            return False

        if Type.type_table.get(_type.signature) is None:
            Type.type_table.__setitem__(_type.signature, _type)
        else:
            print('Already registered type: ' + _type.name)

    def isinstance(self, _value):
        pass

Type.register_type(Type('integer', 'i'))
Type.register_type(Type('string', 's'))
Type.register_type(Type('float', 'f'))
Type.register_type(Type('Object', 'cObject'))

class Variable:
    def __init__(self, _context: TranslationUnit, _type: Type, _name: str,
                 _value=None):
        self.context = _context
        self.type = _type
        self.name = _name
        self.value = _value

    def assign(self, _value):
        if isinstance(_value, Variable):
            return self.copy_assignment(_value)

        if self.type.isinstance(_value):
            self.value = _value
            print(self.name + ' = ' + _value)
            return True

        return False

    def copy_assignment(self, value):
        if value.type == self.type:
            self.value = value.value
            print(self.name + ' = ' + str(value.value))


v = Variable(None, Type.type_table['i'], 'var1', _value=0x0BADF00D)
v.assign(Variable(None, Type.type_table['i'], '', _value=5))
pass
