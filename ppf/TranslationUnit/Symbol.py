import enum

__author__ = 'Maxime Jeanson'

class SymbolType(enum.Enum):
    undefined = 0
    variable = 1
    parameter = 2
    function = 3
    Rule = 4

class Symbol:
    """
    :type name: str
    :type signature: str
    :type type: ppf.TranslationUnit.Symbol.SymbolType
    :type node: None or ppf.ast.ASTNode.ASTNode
    """

    def __init__(self, _name, _signature, _type, _node):
        """

        :type _name: str
        :type _signature: str
        :type _type: ppf.TranslationUnit.Symbol.SymbolType
        :type _node: None or ppf.ast.ASTNode.ASTNode
        """
        self.name = _name
        self.signature = _signature
        self.type = _type
        self.node = _node
