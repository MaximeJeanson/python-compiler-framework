__author__ = 'Maxime Jeanson'

class ASTNode:
    """

    :type attributes: list[str]
    :type children: list[ppf.ast.ASTNode.ASTNode or ppf.ast.Token.Token]
    :type namespace: str
    :type parent: None or ppf.ast.ASTNode.ASTNode
    :type parser: None or ppf.Parser.Parser
    :type tag: str
    :type tokens: list[ppf.ast.Token.Token]
    :type value: str
    """
    def __init__(self, parent, tag, children, parser, value=''):
        """

        :type parent: None or ppf.ast.ASTNode.ASTNode
        :type tag: str
        :type children: list[ppf.ast.ASTNode.ASTNode or ppf.ast.Token.Token]
        :type parser: None or ppf.Parser.Parser
        :type value: str
        """
        self.parser = parser
        self.parent = parent
        self.tag = tag
        self.value = value
        self.children = children
        self.attributes = ['namespace']
        self.namespace = ''
        self.tokens = None

    def post_init(self, tokens):
        """

        :type tokens: list[ppf.ast.Token.Token]
        """
        self.tokens = tokens

    def code_gen(self, device):
        """

        :param device: io.IOBase
        """
        pass
