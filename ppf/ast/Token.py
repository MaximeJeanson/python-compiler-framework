__author__ = 'Maxime Jeanson'

class Token:
    """

    :type type: str
    :type value: str
    :type line: int
    :type column: int
    :type start: int
    :type end: int
    """
    # @seg(9)
    def __init__(self, t='', value='', line=0, column=0, start=0, end=0):
        """

        :type t: str
        :type value: str
        :type line: int
        :type column: int
        :type start: int
        :type end: int
        """
        self.type = t
        self.value = value
        self.line = line
        self.column = column
        self.start = start
        self.end = end
