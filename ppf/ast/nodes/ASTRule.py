import io

from ppf import Parser
from ppf.ast import Token
from ppf.ast import ASTNode

class ASTRule(ASTNode):
    @property
    def context_name(self) -> str:
        return 'Rule'

    def __init__(self, parent: ASTNode, parser: Parser, value: str=''):
        """
        Construct an ASTRule ASTNode object.
        
        :param ASTNode parent: Parent Node in the ast.
        :param str value: The node value.
        """
        super().__init__(parent, 'rule', [], parser, value)
        self.parser.context.append(self.context_name)
        self.namespace = parent.namespace

    def post_init(self, tokens: [Token]):
        """
        Calls after the initialization phase. When a node is done, all it's
        children's post_init method will be call.
        """
        super().post_init(tokens)

        self.value = self.tokens[0].value
        if self.parser.context[-1] is not self.context_name:
            print('error...')
            return

        self.parser.context.pop()

    def code_gen(self, device: io.IOBase):
        """
        This method generate the backend code.
        """
        class_name = 'ast'
        for word in self.value.split('_'):
            class_name += word.capitalize()

        with io.open('out2/' + class_name + '.py', mode='w+') as dev:
            self.generate_ast_node(class_name, dev)

        self.generate_parser_rule(class_name, device)

    def generate_parser_rule(self, class_name: str, device: io.IOBase):
        device.writelines([u'(\'', self.value, u'\', '])

        for child in self.children:
            child.generate_parser_rule(device)

        device.writelines([class_name, ')\n'])

    def generate_ast_node(self, class_name: str, device: io.IOBase):
        device.writelines([
            'from ppf.ast.Token import Token\n',
            'from ppf.ast.ASTNode import ASTNode\n',
            'from io import IOBase\n',
            '\n',
            'class ' + class_name + '(ASTNode):\n',
            '    def __init__(self, parent: ASTNode, value: str=""):\n',
            '        """\n',
            '        Construct an ' + class_name + ' ASTNode object.\n',
            '        \n',
            '        :param ASTNode parent: Parent Node in the ast.\n',
            '        :param str value: The node value.\n',
            '        """\n',
            '        super().__init__(parent, \'' + self.value + '\', [], value)\n',
            '        pass\n',
            '\n',
            '    def post_init(self, tokens: [Token]):\n',
            '        """\n',
            '        Calls after the initialization phase. When a node is done, all it\'s\n'
            '        children\'s post_init method will be call.\n',
            '        """\n',
            '        super().post_init(tokens)\n',
            '        pass\n',
            '\n',
            '    def code_gen(self, device: IOBase):\n',
            '        """\n',
            '        This method generate the backend code.\n',
            '        """\n',
            '        super().code_gen(device)\n',
            '        pass\n',
            '\n'
        ])

    def __children_parser_rules__(self):
        pass
