from ppf import Parser
from ppf.ast import ASTNode
from ppf.ast import Token

class ASTSectionName(ASTNode):
    @property
    def context_name(self) -> str:
        return self._context_name

    def __init__(self, parent: ASTNode, parser: Parser, value: str=''):
        """
        Construct an ASTSection ASTNode object.

        :param ASTNode parent: Parent Node in the ast.
        :param str value: The node value.
        """
        super().__init__(parent, 'section_name', [], parser, value)
        self._context_name = ''
        pass

    def post_init(self, tokens: [Token]):
        """
        Calls after the initialization phase. When a node is done, all it's
        children's post_init method will be call.
        """
        super().post_init(tokens)

        self.value = self.tokens[0].value
        self.parent.context_name = self.value
        self.parser.context.append(self.value)

        pass

    def code_gen(self):
        """
        This method generate the backend code.
        """
        pass

