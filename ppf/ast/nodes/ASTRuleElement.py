from io import IOBase
from ppf.ast.nodes.ASTRuleMultiplicity import ASTRuleMultiplicity
from ppf.ast.Token import Token
from ppf.ast.ASTNode import ASTNode

class ASTRuleElement(ASTNode):
    def __init__(self, parent: ASTNode, value: str=''):
        """
        Construct an ASTRuleElement ASTNode object.
        
        :param ASTNode parent: Parent Node in the ast.
        :param str value: The node value.
        """
        super().__init__(parent, 'rule_element', [], value)
        self.multiplicity = ''
        self.attributes.append('multiplicity')
        pass

    def post_init(self, tokens: [Token]):
        """
        Calls after the initialization phase. When a node is done, all it's
        children's post_init method will be call.
        """
        super().post_init(tokens)

        self.value = self.tokens[0].value
        if len(self.children) > 0:
            self.multiplicity = self.children[0].value
            self.children.pop(0)

    def code_gen(self):
        """
        This method generate the backend code.
        """
        pass

    def generate_parser_rule(self, device: IOBase):
        has_multiplicity = hasattr(self,
                                   'multiplicity') and self.multiplicity is not ''
        if has_multiplicity:
            device.writelines([u'Multiplicity('])

        result = self.is_rule(self.value)

        if result[0]:
            device.writelines([u'Ref(\'', result[1].namespace, self.value, u'\'), '])
        else:
            device.writelines([u'\'', self.value, u'\', '])

        if has_multiplicity:
            device.writelines(
                [ASTRuleMultiplicity.MULTIPLICITY[self.multiplicity], '), '])

        for child in self.children:
            child.generate_parser_rule(device)

    def is_rule(self, name: str):
        for rule in self.parser.parserState.ast.children:
            if rule.value == name:
                return True, rule

        return False, None
