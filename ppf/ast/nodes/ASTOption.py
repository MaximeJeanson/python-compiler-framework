from io import IOBase
from ppf.ast.Token import Token
from ppf.ast.ASTNode import ASTNode

class ASTOption(ASTNode):
    def __init__(self, parent: ASTNode, value: str=''):
        """
        Construct an ASTOption ASTNode object.
        
        :param ASTNode parent: Parent Node in the ast.
        :param str value: The node value.
        """
        super().__init__(parent, 'option', [], value)
        pass

    def post_init(self, tokens: [Token]):
        """
        Calls after the initialization phase. When a node is done, all it's
        children's post_init method will be call.
        """
        super().post_init(tokens)

        self.value = self.tokens[1].value

        pass

    def code_gen(self, device: IOBase):
        """
        This method generate the backend code.
        """
        pass

    def generate_parser_rule(self, device: IOBase):
        pass
