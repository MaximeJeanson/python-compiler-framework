from ppf.ast.Token import Token
from ppf.ast.ASTNode import ASTNode

class ASTArgs(ASTNode):
    def __init__(self, parent: ASTNode, value: str=''):
        """
        Construct an ASTArgs ASTNode object.
        
        :param ASTNode parent: Parent Node in the ast.
        :param str value: The node value.
        """
        super().__init__(parent, 'args', [], value)
        pass

    def post_init(self, tokens: [Token]):
        """
        Calls after the initialization phase. When a node is done, all it's
        children's post_init method will be call.
        """
        super().post_init(tokens)
        pass

    def code_gen(self):
        """
        This method generate the backend code.
        """
        pass

