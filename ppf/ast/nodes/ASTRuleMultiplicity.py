from ppf.ast.Token import Token
from ppf.ast.ASTNode import ASTNode

class ASTRuleMultiplicity(ASTNode):
    MULTIPLICITY = {
        '*': '0, -1',
        '+': '1, -1',
        '?': '0, 1'
    }

    def __init__(self, parent: ASTNode, value: str=''):
        """
        Construct an ASTRuleMultiplicity ASTNode object.
        
        :param ASTNode parent: Parent Node in the ast.
        :param str value: The node value.
        """
        super().__init__(parent, 'rule_multiplicity', [], value)
        pass

    def post_init(self, tokens: [Token]):
        """
        Calls after the initialization phase. When a node is done, all it's
        children's post_init method will be call.
        """
        super().post_init(tokens)

        self.value = self.tokens[0].value

        pass

    def code_gen(self):
        """
        This method generate the backend code.
        """
        pass

