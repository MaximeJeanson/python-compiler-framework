from io import IOBase

from ppf import Parser
from ppf.ast import ASTNode
from ppf.ast import Token

__author__ = 'maxime'

class ASTImport(ASTNode):
    def __init__(self, parent: ASTNode, parser: Parser, value: str=''):
        """
        Construct an ASTSection ASTNode object.

        :param ASTNode parent: Parent Node in the ast.
        :param str value: The node value.
        """
        super().__init__(parent, 'import', [], parser, value)
        pass

    def post_init(self, tokens: [Token]):
        """
        Calls after the initialization phase. When a node is done, all it's
        children's post_init method will be call.
        """
        super().post_init(tokens)
        self.value = self.tokens[1].value
        pass

    def code_gen(self, device: IOBase):
        """
        This method generate the backend code.
        """
        pass

