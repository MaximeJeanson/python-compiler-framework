from ppf.ast.Token import Token
from ppf.ast.ASTNode import ASTNode

class ASTRuleBlock(ASTNode):
    def __init__(self, parent: ASTNode, value: str=''):
        """
        Construct an ASTRuleBlock ASTNode object.
        
        :param ASTNode parent: Parent Node in the ast.
        :param str value: The node value.
        """
        super().__init__(parent, 'rule_block', [], value)
        pass

    def post_init(self, tokens: [Token]):
        """
        Calls after the initialization phase. When a node is done, all it's
        children's post_init method will be call.
        """
        super().post_init(tokens)

        self.children[0].multiplicity = self.children[1].value
        self.children[0].attributes.append('multiplicity')
        index = self.parent.children.index(self)
        self.parent.children[index] = self.children[0]
        self.children[0].parent = self.parent

        pass

    def code_gen(self):
        """
        This method generate the backend code.
        """
        pass

