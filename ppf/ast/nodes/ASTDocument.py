from io import IOBase

from ppf.ast import Token
from ppf.ast import ASTNode

class ASTDocument(ASTNode):
    def __init__(self, parent: ASTNode, value: str=''):
        """
        Construct an ASTDocument ASTNode object.
        
        :param ASTNode parent: Parent Node in the ast.
        :param str value: The node value.
        """
        super().__init__(parent, 'document', [], value)
        self.namespace = '/'
        pass

    def post_init(self, tokens: [Token]):
        """
        Calls after the initialization phase. When a node is done, all it's
        children's post_init method will be call.
        """
        super().post_init(tokens)
        pass

    def code_gen(self, device: IOBase):
        """
        This method generate the backend code.
        """
        for child in self.children:
            child.code_gen(device)
        pass

