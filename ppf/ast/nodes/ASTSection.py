from io import IOBase

from ppf import Parser
from ppf.ast import ASTNode
from ppf.ast import Token

class ASTSection(ASTNode):
    def __init__(self, parent: ASTNode, parser: Parser, value: str=''):
        """
        Construct an ASTSection ASTNode object.
        
        :type parser: ppf.AST.Parser.Parser
        :param ASTNode parent: Parent Node in the ast.
        :param str value: The node value.
        """
        super().__init__(parent, 'section', [], parser, value)
        self.context_name = ''
        self.namespace = parent.namespace
        pass

    def post_init(self, tokens: [Token]):
        """
        Calls after the initialization phase. When a node is done, all it's
        children's post_init method will be call.
        """
        super().post_init(tokens)

        self.value = self.children[0].value
        if self.parser.context[-1] is not self.context_name:
            print('error...')
            return

        self.parser.context.pop()
        self.children.pop(0)
        self.value = self.tokens[1].value

        statements = self.children.pop(0)
        for child in statements.children:
            child.parent = self
            child.namespace = self.namespace + self.value + '/'
            self.children.append(child)

    def code_gen(self, device: IOBase):
        """
        This method generate the backend code.
        """
        for child in self.children:
            child.code_gen(device)
