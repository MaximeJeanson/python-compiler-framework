from io import IOBase
from ppf.ast.nodes.ASTRuleMultiplicity import ASTRuleMultiplicity
from ppf.ast.Token import Token
from ppf.ast.ASTNode import ASTNode

class ASTRuleGroup(ASTNode):
    def __init__(self, parent: ASTNode, value: str=''):
        """
        Construct an ASTRuleGroup ASTNode object.

        :param ASTNode parent: Parent Node in the ast.
        :param str value: The node value.
        """
        super().__init__(parent, 'rule_group', [], value)
        pass

    def post_init(self, tokens: [Token]):
        """
        Calls after the initialization phase. When a node is done, all it's
        children's post_init method will be call.
        """
        super().post_init(tokens)
        if len(self.children) == 1:
            self.parent.children.remove(self)
            self.parent.children.append(self.children[0])
            self.children[0].parent = self.parent

        pass

    def code_gen(self):
        """
        This method generate the backend code.
        """
        pass

    def generate_parser_rule(self, device: IOBase):
        has_multiplicity = hasattr(self,
                                   'multiplicity') and self.multiplicity is not ''
        if has_multiplicity:
            device.writelines([u'Multiplicity('])

        device.writelines([u'And(['])

        for child in self.children:
            child.generate_parser_rule(device)

        device.writelines([u']), '])

        if has_multiplicity:
            device.writelines(
                [ASTRuleMultiplicity.MULTIPLICITY[self.multiplicity],
                 '), '])
