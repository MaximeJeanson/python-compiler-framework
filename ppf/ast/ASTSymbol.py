from ppf.ast.ASTNode import ASTNode
from ppf.ast.Token import Token

__author__ = 'maxime'

class ASTSymbol(ASTNode):
    def __init__(self, parent: ASTNode or None, tag: str, children: ['ASTNode' or Token], parser: 'Parser', value: str=''):
        super(parent, )
