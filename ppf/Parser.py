import re

from .ast import ASTNode, Token
from .group import BaseContext, Group, Ref
from .LexerState import LexerState

__author__ = 'Maxime Jeanson'

class ParserState:
    # @seg(9)
    def __init__(self):
        self.ast = ASTNode(None, 'ROOT', [], None)
        self.context = self.ast


class Parser:
    # @seg(9)
    def __init__(self, tokens, keywords, rules):
        super().__init__()
        self.tokens = tokens
        self.keywords = keywords
        self.rules = rules
        self.lexerState = None
        self.parserState = ParserState()
        self.context = ['root']

        for rules in Ref.REGISTERED_REF:
            rules.post_init(self)

    # TODO: Move the Lexer part of the Parser into a Lexer...
    # @seg(3)
    def __tokenize__(self, pos):
        token = None
        try:
            token = self.lexerState.regex.match(self.lexerState.code, pos=pos)
        except StopIteration:
            return None

        if token is None:
            raise EOFError()

        kind = token.lastgroup
        value = token.group(kind)
        start = token.start()
        end = token.end()
        column = token.start() - self.lexerState.line_start

        if kind == 'NL':
            self.lexerState.line_start = token.end()
            self.lexerState.line_num += 1
        elif kind == 'SKIP':
            pass
        elif kind == 'IGNORED':
            # TODO: Adding a compilation flag for this warning and a way to set the level.
            l = self.lexerState.code[self.lexerState.line_start:-1].split('\n')[0]
            # TODO: Use LLVM error message (For compatibility reason with already in-place software)
            # NOTE:   t.c:5:28: warning: use of GNU old-style field designator extension << BOLD
            # NOTE:   struct point origin = { x: 0.0, y: 0.0 };
            # NOTE:                           ~~ ^                                       << GREEN
            # NOTE:                           .x =                                       << BOLD, GREEN

            # TODO: Use a translation layer for error reporting.
            print('[ WARNING ] Skipping unknown token : l %d c %d' % (self.lexerState.line_num, column + 1))
            print('            %s' % l)
            print(format(('            {0:.<%d}{0:^<%d}{0:.<%d}\n' % (column, end - start, len(l) - (column + (end - start)))).format('')), end='')
            pass
        elif kind == 'COMMENT':
            # TODO: This is not working in all contexts (Language can define comments as C_COMMENT for C style comments...) Have to be handle by the parser as a global token (token that can happen in any context).
            pass
        else:
            if kind == 'ID' and value in self.keywords:
                # TODO: should be handled in the same way as Flex does. (Code per token declaration)
                kind = value
                value = None

            t = Token(kind, value, self.lexerState.line_num, column, start, end)

            if len(self.lexerState.code) + 1 <= end:
                return None
            return t
        try:
            return self.__tokenize__(end)
        except EOFError:
            raise EOFError

    # @seg(5)
    def tokenize(self, code):
        tok_regex = '|'.join('(?P<%s>%s)#%s\n' % pair for pair in self.tokens)
        regex = re.compile(tok_regex, flags=re.VERBOSE)
        line_num = 1
        line_start = 0

        it = iter(re.finditer(tok_regex, code, flags=re.VERBOSE))

        self.lexerState = LexerState(tok_regex, line_num, line_start, it, code, regex)

    # @seg(3)
    def parse_next(self, _entry: object, _context: BaseContext):
        tokens_len = len(_context.tokens)
        if _context.token_index >= tokens_len:
            t = None
            try:
                t = self.__tokenize__(_context.tokens[_context.token_index - 1].end)
            except (EOFError, IndexError):
                if _context.token_index >= tokens_len:
                    return BaseContext(None, self, _context.tokens, -1, _context.token_start, _context.token_start, False)

            if t is not None:
                if _context.token_index >= tokens_len:
                    _context.tokens.append(t)
                else:
                    _context.tokens[_context.token_index] = t

        return self.__parse_next__(_context, _entry)

    def __parse_next__(self, _context, _entry):
        if isinstance(_entry, Group):
            return _entry.parse(_context, _context.token_index)
        match = _context.tokens[_context.token_index].type == _entry
        return BaseContext(None, self, _context.tokens, -1,
                           _context.token_index + 1 if match else 0,
                           _context.token_start, match)

    # @seg(5)
    def parse(self, code):
        tokens = []

        self.tokenize(code)

        try:
            t = self.__tokenize__(0)

            if t is not None:
                tokens.append(t)
        except EOFError:
            pass

        rule = self.rules['root'][0]
        r = Ref(rule[0])

        result = r.parse(BaseContext(None, self, tokens, -1, 0, 0, False), 0)

        self.parserState.ast = self.parserState.ast.children[0]

        # if __debug__:
        # ppf.print_level(0, self.parserState.ast)
