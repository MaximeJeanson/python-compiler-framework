from .Group import Group
from .BaseContext import BaseContext
from .Ref import Ref

__author__ = 'Maxime Jeanson'

class Multiplicity(Group):
    # @seg(9)
    def __init__(self, item, start, end):
        self.item = item
        self.start = start
        self.end = end

        if start < 0:
            raise AttributeError("start is negative")

    # @seg(7)
    @staticmethod
    def is_valid_within_context(_context: BaseContext, context: BaseContext):
        if not _context.parent:
            return True

        valid = _context.parent.parse(context, context.token_index,
                                      _context.rule_index)

        if valid.match:
            _context.rule_index = valid.rule_index
            context.token_index = valid.token_index
            return True

        return False

    # @seg(7)
    def rollback(self, _context: BaseContext, context: BaseContext):
        context.match = self.is_valid_within_context(_context, context)
        if context.match:
            return True

        while context.pop():
            context.match = self.is_valid_within_context(_context, context)

            if context.match:
                return True

        return False

    # @seg(3)
    def parse(self, _context: BaseContext, _token_start: int,
              _rule_index: int=0) -> BaseContext:
        context = BaseContext(self, _context.parser_instance, _context.tokens,
                              -1, _token_start, _token_start, True)
        for index in range(0, self.start):
            result = context.parser_instance.parse_next(self.item, context)
            self.handle_result(context, result)
            context.match = context.match and result.match
        if self.end < self.start:
            index = self.start + 1
            while True:
                result = self.parse_next(context)
                if not result.match:
                    if isinstance(_context.parent, Ref):
                        break

                    self.rollback(_context, context)
                    break

                index += 1
        else:
            for index in range(self.start, self.end):
                result = self.parse_next(context)
                if not result.match:
                    context.match = index - 1 < self.start

                    if isinstance(_context.parent, Ref):
                        break

                    self.rollback(_context, context)
                    break

        return context

    # @seg(7)
    def parse_next(self, context):
        # TODO: Find a way to improve performance (we don't need to push all the time the current context).
        context.push()
        result = context.parser_instance.parse_next(self.item, context)
        self.handle_result(context, result)
        return result
