from .Group import Group
from .BaseContext import BaseContext

__author__ = 'Maxime Jeanson'

class And(Group):
    # @seg(9)
    def __init__(self, items):
        self.items = items
        self.items_len = len(items)

    # @seg(3)
    def parse(self, _context, _token_start, _rule_index=0):
        context = BaseContext(self, _context.parser_instance, _context.tokens,
                              _rule_index, _token_start, _token_start, True)

        while context.rule_index < self.items_len and context.match:
            entry = self.items[context.rule_index]  # entry: The current item to match.
            context.rule_index += 1
            result = context.parser_instance.parse_next(entry, context)
            self.handle_result(context, result)
            context.match = context.match and result.match

        return context

