from .BaseContext import BaseContext
from .Group import Group

class Ref(Group):
    """
    Represent a reference to a rule of the parser.

    :type item: str
    :type context: list[str]
    :type rule: tuple|None
    :type REGISTERED_REF: list[Ref]
    """

    REGISTERED_REF = []

    def __init__(self, item: str, context: [] or None=None):
        """
        Construct a reference to the given rule within the given context.

        :param item: The referenced rule name.
        :param context: List of the contexts in which this rule can be found.


        """
        self.item = item
        self.context = context
        self.rule = None

        if context is None:
            ctx = self.item.split('/')
            if len(ctx) > 1:
                self.context = ['root']
                for namespace in ctx[0:-1]:
                    if namespace:
                        self.context.append(namespace)
            self.item = ctx[-1]

        Ref.REGISTERED_REF.append(self)

    # TODO: Add a static function that call this method (should be private as
    #       well) so the parser don't have to call it explicitly. Then it should
    #       clean the Ref.REGISTERED_REF
    def post_init(self, parser_instance):
        """

        :param parser_instance:
        :type parser_instance: ppf.Parser.Parser
        """
        if self.context:
            for context_index in range(1, len(self.context) + 1):
                for rule in parser_instance.rules[self.context[-context_index]]:
                    if rule[0] == self.item:
                        self.rule = rule
            # TODO: Add an exception for rule not found here.

    # @seg(3)
    # @profile
    def parse(self, _context, _token_start, _rule_index=0):
        """

        :type _context: ppf.group.BaseContext.BaseContext
        :type _token_start: int
        :type _rule_index: int
        :rtype: ppf.group.BaseContext.BaseContext
        """
        context = BaseContext(self, _context.parser_instance, _context.tokens, _rule_index, _token_start, _token_start, True)

        parser_context = self.__parser_context__(context)

        if self.rule:
            self.__parse_rule__(context, self.rule)
        else:
            for context_index in range(1, len(parser_context) + 1):
                rules = context.parser_instance.rules[parser_context[-context_index]]
                for rule in rules:
                    if rule[0] == self.item:
                        return self.__parse_rule__(context, rule)

        return context

    # @profile
    def __parse_rule__(self, context, rule):
        """

        :param context:
        :param rule:
        :return:

        :type context: ppf.group.BaseContext.BaseContext
        :type rule: tuple|None
        :rtype: ppf.group.BaseContext.BaseContext
        """
        parser_instance = context.parser_instance

        saved_ast_context = parser_instance.context.copy()
        ast_context = parser_instance.parserState.context

        parser_instance.parserState.context = rule[2](ast_context, parser_instance)
        saved_context = parser_instance.parserState.context

        result = rule[1].parse(context, context.token_index)
        context.match = result.match
        self.handle_result(context, result)

        if result.match:
            ast_context.children.append(saved_context)
            saved_context.post_init(context.tokens[context.token_start:result.token_index])
        else:
            parser_instance.context = saved_ast_context

        parser_instance.parserState.context = ast_context
        return context

    def __parser_context__(self, context):
        """

        :param context:
        :type context: ppf.group.BaseContext.BaseContext
        :return:
        :rtype: list[str]
        """
        if not self.context:
            return context.parser_instance.context

        return self.context
