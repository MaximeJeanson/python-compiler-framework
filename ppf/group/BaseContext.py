from copy import copy

# from utils.seg import seg

__author__ = 'maxime'

class BaseContext:
    """

    :type parser_instance: ppf.Parser.Parser
    :type match: bool
    :type token_start: int
    :type token_index: int
    :type rule_index: int
    :type tokens: list[ppf.ast.Token.Token]
    :type parent: ppf.group.Group.Group
    :type next: ppf.group.BaseContext.BaseContext|None
    """
    # @seg(9)
    def __init__(self, parent, parser_instance, tokens, rule_index, token_index,
                 token_start, match):
        """

        :type parent: None or ppf.group.Group.Group
        :type parser_instance: ppf.Parser.Parser
        :type tokens: list[Token]
        :type rule_index: int
        :type token_index: int
        :type token_start: int
        :type match: bool
        """
        self.parser_instance = parser_instance
        self.match = match
        self.token_start = token_start
        self.token_index = token_index
        self.rule_index = rule_index
        self.tokens = tokens
        self.parent = parent
        self.next = None

    # @seg(5)
    def push(self):
        self.next = copy(self)

    # @seg(5)
    def pop(self):
        if not self.next:
            return False

        self.parser_instance = self.next.parser_instance
        self.match = self.next.match
        self.token_start = self.next.token_start
        self.token_index = self.next.token_index
        self.rule_index = self.next.rule_index
        self.tokens = self.next.tokens
        self.parent = self.next.parent
        self.next = self.next.next

        return True
