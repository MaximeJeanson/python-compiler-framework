"""

"""
__author__ = 'maxime'

class Group:
    """

    """
    def parse(self, _context: 'BaseContext', _token_start: int,
              _rule_index: int=0) -> 'BaseContext':
        """

        :param _context:
        :type _context:
        :param _token_start:
        :type _token_start:
        :param _rule_index:
        :type _rule_index:
        :return:
        :rtype:
        """
        pass

    @staticmethod
    def handle_result(_context: 'BaseContext', _result: 'BaseContext'):
        """

        :param _context:
        :type _context:
        :param _result:
        :type _result:
        :return:
        :rtype:
        """
        _context.token_index = _result.token_index if _result.match else _result.token_start
