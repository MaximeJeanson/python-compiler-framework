__author__ = 'maxime'


class LexerState:
    def __init__(self, tok_regex, line_num, line_start, it, code, regex):
        """

        :type tok_regex: str
        :type line_num: int
        :type line_start: int
        :type it:
        :type code:  str
        :type regex: re.__Regex
        """
        super().__init__()

        self.tok_regex = tok_regex
        self.line_num = line_num
        self.line_start = line_start
        self.iter = it
        self.code = code
        self.regex = regex
