from ppf.ast.ASTNode import ASTNode
from ppf.ast.nodes.ASTRule import ASTRule
from ppf.TranslationUnit.Scope import Scope
from ppf.TranslationUnit.Symbol import Symbol, SymbolType

__author__ = 'maxime'

class NamespacePass:
    @staticmethod
    def run(node: ASTNode, scope: Scope):
        for child in node.children:
            if isinstance(child, ASTRule):
                scope.symbols.add(Symbol(child.value, child.namespace + child.value, SymbolType.Rule, child))
