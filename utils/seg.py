import inspect
import io

__author__ = 'maxime'

if __debug__:
    diag = io.open('out.diag', mode='w+')

    diag.write('diagram {\n')
    diag.write('span_height = 0;\n')
    diag.write('span_width = 5;\n')
    diag.write('default_fontsize = 12;\n')
    diag.write('NULL; Parser; Ref; Multiplicity; Or; And;\n\n')

    __verbosity__ = 3

def caller_name(skip=2, mod: bool=True, clazz: bool=True, func: bool=True):
    """Get a name of a caller in the format module.class.method

       `skip` specifies how many levels of stack to skip while getting caller
       name. skip=1 means "who calls me", skip=2 "who calls my caller" etc.

       An empty string is returned if skipped levels exceed stack height
    """
    stack = inspect.stack()
    start = 0 + skip
    if len(stack) < start + 1:
        return ''
    parentframe = stack[start][0]

    name = []
    module = inspect.getmodule(parentframe)
    # `modname` can be None when frame is executed directly in console
    # TODO(techtonik): consider using __main__
    if module and mod:
        name.append(module.__name__)
    # detect classname
    if 'self' in parentframe.f_locals and clazz:
        # I don't know any way to detect call from the object method
        # XXX: there seems to be no way to detect static method call - it will
        #      be just a function call
        name.append(parentframe.f_locals['self'].__class__.__name__)
    codename = parentframe.f_code.co_name
    if codename != '<module>' and func:  # top level usually
        name.append( codename ) # function or a method
    del parentframe
    return ".".join(name) if len(name) > 0 else 'NULL'

def seg(level: int=2):
    def entangle(f: classmethod):
        def inner(*args, **kwargs):
            if not __debug__ or level > __verbosity__:
                return f(*args, **kwargs)

            mods = f.__module__.split('.')
            diag.write('%s  -> %s [label="%s"];\n' % (caller_name(2, mod=False, func=False), mods[len(mods) - 1], f.__name__))
            result = f(*args, **kwargs)
            diag.write('%s <-- %s [label="return"];\n' % (caller_name(2, mod=False, func=False), mods[len(mods) - 1]))
            diag.flush()
            return result
        return inner
    return entangle
