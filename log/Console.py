import sys

__author__ = 'maxime'


class Console:
    normal = ""
    good = "\x1b[32;1m"
    warning = "\x1b[33;1m"
    error = "\033[31;1m"
    end = "\x1b[0m"

    @staticmethod
    def out(message_type, message):
        if sys.platform is 'win32' or message_type is Console.normal:
            print(message, end='')
        else:
            print(message_type + message + Console.end, end='')

        return Console
