import argparse
import io
import os
import re
import collections

from ppf import Parser
from ppf.group import And
from ppf.group import Multiplicity
from ppf.group import Or
from ppf.group import Ref
from log.Console import Console

__author__ = 'maxime'

files = set()
files_imports = dict()
sizes = dict()
outFile = ''
parser = argparse.ArgumentParser(
    description='Merge multiple files into one using a dependency resolver.')
parser.add_argument('inputFileName', metavar='INPUT FILE', type=str, nargs='+',
                    help='The input file name to parse.')
parser.add_argument('outputFileName', metavar='OUTPUT FILE', type=str, nargs=1,
                    help='The generated file name.')
parser.add_argument('-l', '--licence', metavar='LICENCE FILE', type=str,
                    nargs=1, help='The licence file to include.')
pattern = re.compile(r'^// @import (.+);\n', re.MULTILINE)
sectionFile = io.open("sections.cfg")
sections = []
section_headers = []
defined = dict()

section_contents = []

args = parser.parse_args()

# sys.platform = 'win32'

# Token = collections.namedtuple('Token', ['typ', 'value', 'line', 'column', 'start', 'end'])
ASTElement = collections.namedtuple('ASTElement', ['name', 'values'])


def on_import(value, vals):
    print("IMPORT: " + value)
    for val in vals:
        if val.value is None:
            print(" - " + val.type)
        else:
            print(" - " + val.value)


def on_section(value, vals):
    print("SECTION: " + value)
    for val in vals:
        if val.value is None:
            print(" - " + val.type)
        else:
            print(" - " + val.value)


def on_define(value, vals):
    print("DEFINED: " + value)
    for val in vals:
        if val.value is None:
            print(" - " + val.type)
        else:
            print(" - " + val.value)


def on_id(value, vals):
    print("Warning: Undefined id: " + value)
    for val in vals:
        if val.value is None:
            print(" - " + val.type)
        else:
            print(" - " + val.value)


def on_var(value, vals):
    print("VAR: " + value)
    for val in vals:
        if val.value is None:
            print(" - " + val.type)
        else:
            print(" - " + val.value)


def on_const(value, vals):
    print("CONST: " + value)
    for val in vals:
        if val.value is None:
            print(" - " + val.type)
        else:
            print(" - " + val.value)


pre = Parser([
    ('NUMBER', r'\d+(\.\d+)?', True),
    ('DQ_STRING', r'"(?:(?:\\")|[\s\S])*?"', True),
    ('SQ_STRING', r'\'(?:(?:\\\')|[\s\S])*?\'', True),
    ('EQ', r'=', False),
    ('END', r';', False),
    ('ID', r'[a-zA-Z_]+', True),
    ('NL', r'\n', False),
    ('SKIP', r'[ \t]+', False),
    ('IGNORED', r'.', False),
], {
    'define', 'import', 'section', 'var', 'const'
}, [
    ('IMPORT', And(['import', 'SQ_STRING', 'END']), on_import),
    ('SECTION', And(['section', And(['ID']), 'END']), on_section),
    ('DEFINE', And(['define', 'ID', 'EQ', 'SQ_STRING', 'END']), on_define),
    ('VAR', And(['var', 'ID', Multiplicity(And(['EQ', Or(['ID', Ref('STRING')]),
                                                'END']), 0, 1)]), on_var),
    ('CONST', And(['const', 'ID', Multiplicity(And(['EQ', Or(['ID',
                                                              Ref('STRING')]),
                                                    'END']), 0, 1)]), on_const),
    ('UNDEFINED_ID', And(['ID']), on_id),
    ('STRING', Or(['DQ_STRING', 'SQ_STRING']))
])

for input_file_name in args.inputFileName:
    print("FILE: " + input_file_name)
    with io.open(input_file_name) as f:
        pre.parse(f.read())
        # print(token)


def find_imports(filename):
    if files.__contains__(filename):
        return 0

    files.add(filename)
    files_imports[filename] = []

    with io.open(filename) as file:
        file_content = file.read()

    import_matches = re.findall(r'// @import (.+);\n', file_content,
                                re.MULTILINE)

    for path in import_matches:
        if os.path.exists("./" + path):
            if path == filename:
                print("Warning: Recursive import. Skipping " + path)
            else:
                find_imports(path)
                files.add(path)
                files_imports[filename].append(path)
        else:
            print("Warning: File not found: " + path)


def find_defines(filename):
    if files.__contains__(filename):
        return 0

    files.add(filename)
    files_imports[filename] = []

    with io.open(filename) as file:
        file_content = file.read()

    define_matches = re.findall(
        r'//\s*@define\s+([a-zA-Z_&%\$@~#\(\{\[\]\}\)][0-9a-zA-Z_&%\$@~#\(\{\[\]\}\)]+)\s*=\s*(.+)\s*;\n',
        file_content,
        re.MULTILINE)

    for define in define_matches:
        # define.

        if defined.__contains__(define[0]):
            Console.out(Console.warning, "Warning: ").out(Console.normal,
                                                          "Already defined " +
                                                          define[0] + "\n")
        else:
            defined[define[0]] = define[1]


def map_imports():
    sizes.clear()

    for imports in files_imports:
        size = (len(files_imports[imports]))

        if not sizes.__contains__(size):
            sizes[size] = []

        sizes[size].append(imports)

    return sizes


def parse_section_file():
    content = sectionFile.read()

    section_matches = re.findall(r'(?P<name>\w+):\n(?P<header>[^;]+);',
                                 content, re.MULTILINE)

    for section in section_matches:
        sections.append(section[0])
        section_headers.append(section[1])
        section_contents.append([])


def find_section(file, section):
    file.seek(0)
    file_content = file.read()

    section_matches = re.findall(
        r'// @section (\w+);\n+([^\n][\s\S]+?)\n?(?=// @section|\Z)',
        file_content, re.MULTILINE)

    section_content = ""

    for sect in section_matches:
        if sections.__contains__(sect[0]):
            if section == sect[0]:
                if sect[1] is not None:
                    section_content += sect[1]
        else:
            print("Warning: Section not defined: " + section)

    return section_content


def out():
    size = len(sizes)

    for s in sizes:
        size += len(sizes[s])

    s2 = size

    while len(sizes):
        for filename in sizes[0]:
            with open(filename) as inFile:
                for i in range(len(sections)):
                    section_contents[i].append(
                        find_section(inFile, sections[i]))

            for parent in files_imports:
                if files_imports[parent].__contains__(filename):
                    files_imports[parent].remove(filename)

            size -= 1
            Console.out(Console.normal, "[{:>4.0%}]".format((1 - size / s2))) \
                .out(Console.good, " @imported ") \
                .out(Console.normal, filename + "\n")

            files_imports.pop(filename)

        map_imports()

        size = len(sizes)

        for s in sizes:
            size += len(sizes[s])

    for i in range(len(section_headers)):
        outFile.write(section_headers[i] + "\n")
        for content in section_contents[i]:
            content.strip("\n\t ")

            if content is not "":
                outFile.write(content.replace("\n\n\n", "\n\n") + "\n")


Console.out(Console.normal, "[{:>4.0%}]".format(0))
Console.out(Console.good, " @parsing\n")

for input_file_name in args.inputFileName:
    find_imports(input_file_name)
    find_defines(input_file_name)

map_imports()
parse_section_file()

with open(args.outputFileName[0], mode='w+') as outFile:
    if args.licence is not None:
        with open(args.licence[0]) as licenceFile:
            outFile.write(licenceFile.read())

    out()

    Console.out(Console.normal, "[{:>4.0%}]".format(1))
    Console.out(Console.good, " @reformatted\n")
