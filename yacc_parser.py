import argparse
import io
import re
import time

from ppf import Parser
from ppf.group import And, Multiplicity, Or, Ref
from ppf.ast import nodes
from ppf.TranslationUnit import Scope, Symbol, SymbolType, TranslationUnit
from ppf.Passes.NamespacePass import NamespacePass

__author__ = 'Maxime Jeanson'

parser = argparse.ArgumentParser(
    description='Merge multiple files into one using a dependency resolver.')
parser.add_argument('inputFileName', metavar='INPUT FILE', type=str, nargs='+',
                    help='The input file name to parse.')
parser.add_argument('outputFileName', metavar='OUTPUT FILE', type=str, nargs=1,
                    help='The generated file name.')
parser.add_argument('-l', '--licence', metavar='LICENCE FILE', type=str,
                    nargs=1, help='The licence file to include.')
pattern = re.compile(r'^// @import (.+);\n', re.MULTILINE)

args = parser.parse_args()


def initialize():
    return Parser(
        [
            ('COMMENT', r'/\*.*?\*/', False),
            ('DOC_COMMENT', r'/\*\*.*?\*/', False),
            ('NUMBER', r'\d+?', True),
            ('DQ_STRING', r'"(?:(?:\\")|[\s\S])*?"', True),
            ('SQ_STRING', r'\'(?:(?:\\\')|[\s\S])*?\'', True),
            ('COLON', r':', False),
            ('COMMAS', r',', False),
            ('PERCENT', r'\%', False),
            ('LP', r'\(', False),
            ('RP', r'\)', False),
            ('LCB', r'\{', False),
            ('RCB', r'\}', False),
            ('OR', r'\|', False),
            ('PLUS', r'\+', False),
            ('QMARK', r'\?', False),
            ('STAR', r'\*', False),
            ('END', r';', False),
            ('ID', r'[a-zA-Z_\.]+', True),
            ('NL', r'\n', False),
            ('SKIP', r'[ \t]+', False),
            ('IGNORED', r'.', False),
        ], {'import'}, {
            'root': [
                ('document', Multiplicity(
                    Or([Ref('/Rule/rule'), Ref('option'), Ref('import'),
                        Ref('section')]), 0, -1), nodes.ASTDocument),
                ('option', And(['PERCENT', Or([Ref('call')]), 'END']),
                 nodes.ASTOption),
                ('call',
                 And(['ID', 'LP', Multiplicity(Ref('args'), 0, 1), 'RP']),
                 nodes.ASTStatements),
                ('import', And(['import', 'ID', 'END']), nodes.ASTImport),
                ('args', And([Ref('arg'),
                              Multiplicity(And(['COMMAS', Ref('arg')]), 0,
                                           -1)]),
                 nodes.ASTArgs),
                ('arg', Or(['ID', 'NUMBER', 'SQ_STRING', 'DQ_STRING']),
                 nodes.ASTArg),
                ('section', And(
                    [Ref('section_name'), 'ID', 'LCB', Ref('statements'),
                     'RCB']),
                 nodes.ASTSection),
                ('section_name', And(['ID']), nodes.ASTSectionName)
            ],
            'namespace': [
                ('statements', Multiplicity(Ref('/Rule/rule'), 1, -1),
                 nodes.ASTStatements),
            ],
            'Rule': [
                ('rule', And(['ID', 'COLON', Multiplicity(Ref('option'), 0, -1),
                              Ref('Rule/rule_def'), 'END']), nodes.ASTRule),
                ('statements', And([Multiplicity(Ref('Rule/rule_def'), 1, -1)]),
                 nodes.ASTStatements),
                ('rule_def', And([Multiplicity(Ref('Rule/rule_group'), 1, -1),
                                  Multiplicity(And(['OR',
                                                    Multiplicity(
                                                        Ref('Rule/rule_group'), 1,
                                                        -1)]), 0,
                                               -1)]),
                 nodes.ASTRuleDef),
                ('rule_group',
                 Multiplicity(Or([Ref('Rule/rule_element'), Ref('Rule/rule_block')]), 1,
                              -1),
                 nodes.ASTRuleGroup),
                ('rule_block', And(['LP', Ref('Rule/rule_def'), 'RP',
                                    Multiplicity(Ref('Rule/rule_multiplicity'), 0,
                                                 1)]),
                 nodes.ASTRuleBlock),
                ('rule_element',
                 And(['ID', Multiplicity(Ref('Rule/rule_multiplicity'), 0, 1)]),
                 nodes.ASTRuleElement),
                ('rule_multiplicity',
                 Or(['PLUS', 'STAR', 'QMARK', And(['LB', 'NUMBER',
                                                   Multiplicity(And(
                                                       ['COMMAS', 'NUMBER']), 0,
                                                       1),
                                                   'RB'])]),
                 nodes.ASTRuleMultiplicity),
            ]
        }
    )


for input_file_name in args.inputFileName:
    pre = initialize()

    tu = TranslationUnit(input_file_name, pre, pre.parserState.ast)
    # TODO: Replace with LLVM message style.
    print("FILE: " + input_file_name)
    with io.open(input_file_name) as f:
        start = time.process_time()
        pre.parse(f.read())
        end = time.process_time()
        scope = Scope(None, tu, 'doc')
        scope.symbols.__add__(
            Symbol('foo', 'foo', SymbolType.function,
                   pre.parserState.ast.children[0]))
        scope.symbols.__add__(
            Symbol('foo', 'foo', SymbolType.function,
                   pre.parserState.ast.children[0]))
        NamespacePass.run(pre.parserState.ast, scope)

        print('Parsed in %fms' % ((end - start) * 1000))

        pre.parserState.ast.code_gen(io.open('out2/Yacc.py', mode='w+'))
